package idfunds.Services;

import idfunds.Models.Transaction;
import idfunds.Repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {
    @Autowired
    TransactionRepository transactionRepository;

    public Transaction getTransactionByPayerId(int payerId){
        return this.transactionRepository.findByPayerId(payerId).get();
    }

    public Transaction getTransactionByReceiverId(int receiverId){
        return this.transactionRepository.findByReceiverId(receiverId).get();
    }
}
