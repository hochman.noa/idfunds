package idfunds.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @Column(name = "id")
    @JsonProperty
    private Integer id;

    @JsonProperty
    @ManyToOne
    @JoinColumn(name="PayerID", referencedColumnName = "AccountID", insertable = false, updatable = false)
    private Account payer;

    @JsonProperty
    @ManyToOne
    @JoinColumn(name="ReceiverID", referencedColumnName = "AccountID", insertable = false, updatable = false)
    private Account receiver;

    @JsonProperty
    @Column(name = "amount")
    private Double amount;

    @JsonProperty
    @Column(name = "digital_signature")
    private String digitalSignature;
}
