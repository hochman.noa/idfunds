package idfunds.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Blocks")
public class Block {

    @Id
    @Column(name = "BlockID")
    @JsonProperty
    private Integer BlockID;

    @Column(name = "PrevHash")
    @JsonProperty
    private String PrevHash;

    @Column(name = "BlockHash")
    @JsonProperty
    private String BlockHash;

    @Column(name = "BlockProof")
    @JsonProperty
    private String BlockProof;
}
