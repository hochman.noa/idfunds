package idfunds.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "Requests")
public class Request {

    @Id
    @Column(name = "RequestID")
    @JsonProperty
    private Integer RequestID;

    @JsonProperty
    @ManyToOne
    @JoinColumn(name = "PayerID", referencedColumnName = "AccountID", insertable = false, updatable = true)
    private Integer PayerID;

    @JsonProperty
    @JoinColumn(name = "ReceiverID", referencedColumnName = "AccountID", insertable = false, updatable = true)
    private Integer ReceiverID;

    @Column(name = "ApproverID")
    @JsonProperty
    private Integer ApproverID;

    @Column(name = "Amount")
    @JsonProperty
    private Double Amount;

    @Column(name = "Status")
    @JsonProperty
    private String Status;

    @Column(name = "ApproverSignature")
    @JsonProperty
    private String ApproverSignature;

}
