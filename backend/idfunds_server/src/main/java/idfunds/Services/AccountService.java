package idfunds.Services;

import idfunds.Models.Account;
import idfunds.Repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    AccountRepository accountRepository;

    public double findBalanceById(int id){
        Account account = accountRepository.findById(id).get();
        return account.balance();
    }
}
