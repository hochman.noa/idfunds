package idfunds.Controllers;

import idfunds.Services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Accounts")
public class AccountController {
    @Autowired
    AccountService accountService;

    @GetMapping("/balance/{id}")
    public double getBalanceById(@PathVariable int id){
        return accountService.findBalanceById(id);
    }
}
