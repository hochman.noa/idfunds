package idfunds.Repositories;

import idfunds.Models.Account;
import idfunds.Models.Block;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface BlockRepository extends JpaRepository<Block,Integer> {
}
