package idfunds.Repositories;

import idfunds.Models.Account;
import idfunds.Models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
    Optional<Transaction> findByPayerId(int payerId);
    Optional<Transaction> findByReceiverId(int receiverId);
}
