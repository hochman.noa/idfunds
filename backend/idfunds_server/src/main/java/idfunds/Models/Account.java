package idfunds.Models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "Accounts")
public class Account {

    @Id
    @Column(name = "AccountID")
    @JsonProperty
    private Integer id;

    @Column(name = "AccountName")
    @JsonProperty
    private String accountName;

    @Column(name = "AccountDescription")
    @JsonProperty
    private String accountDesc;

    @Column(name = "Balance")
    @JsonProperty
    private Double balance;

    public Double balance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
